//
//  String+Extension.swift
//  LifesumDemo
//
//  Created by Samet Berberoğlu on 2.07.2022.
//

import Foundation

extension String {
    
    var localized: String {
        return NSLocalizedString(self, comment: self)
    }
    
    func localizedWithArguments(_ arguments: CVarArg...) -> String {
        return String(format: self.localized, arguments)
    }
    
}
