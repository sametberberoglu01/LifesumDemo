//
//  Array+Extension.swift
//  LifesumDemo
//
//  Created by Samet Berberoğlu on 2.07.2022.
//

import Foundation

extension Array {
    
    var randomElement: Element? {
        if self.isEmpty { return nil }
        let index = Int.random(in: 0..<self.count)
        return self[index]
    }
    
}
