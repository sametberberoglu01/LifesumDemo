//
//  View+Extension.swift
//  LifesumDemo
//
//  Created by Samet Berberoğlu on 2.07.2022.
//

import SwiftUI

extension View {
    
    func setHidden(_ hidden: Bool) -> some View {
        opacity(hidden ? 0 : 1)
    }
    
}
