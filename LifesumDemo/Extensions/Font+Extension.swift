//
//  Font+Extension.swift
//  LifesumDemo
//
//  Created by Samet Berberoğlu on 2.07.2022.
//

import SwiftUI

extension Font {
    
    static func avenir(weight: Font.Weight, size: CGFloat) -> Font {
        return Font.custom("Avenir", size: size).weight(weight)
    }
    
}
