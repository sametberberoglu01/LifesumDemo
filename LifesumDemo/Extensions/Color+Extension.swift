//
//  Color+Extension.swift
//  LifesumDemo
//
//  Created by Samet Berberoğlu on 2.07.2022.
//

import SwiftUI

extension Color {
    
    enum Custom: String {
        
        ///Light(red:243 green:167 blue:78 alpha:1.0) Dark(red:243 green:167 blue:78 alpha:1.0)
        case ls_orange
        
        ///Light(red:237 green:84 blue:96 alpha:1.0) Dark(red:237 green:84 blue:96 alpha:1.0)
        case ls_grapefruit
        
        ///Light(red:255 green:102 blue:92 alpha:0.33) Dark(red:255 green:102 blue:92 alpha:0.33)
        case ls_orangeyShadow
        
        ///Light(red:108 green:108 blue:108 alpha:1.0) Dark(red:108 green:108 blue:108 alpha:1.0)
        case ls_grayText
        
        ///Light(red:1 green:5 blue:33 alpha:1.0) Dark(red:221 green:221 blue:221 alpha:1.0)
        case ls_primaryButton
        
        ///Light(red:255 green:255 blue:255 alpha:1.0) Dark(red:0 green:0 blue:0 alpha:1.0)
        case ls_primaryButtonText
    }
    
    init(red: Int, green: Int, blue: Int, alpha: CGFloat = 1.0) {
        self.init(UIColor(red: CGFloat(red) / 255.0,
                          green: CGFloat(green) / 255.0,
                          blue: CGFloat(blue) / 255.0,
                          alpha: alpha))
    }
    
    init(white: Int, alpha: CGFloat = 1.0) {
        self.init(UIColor(white: CGFloat(white) / 255.0,
                          alpha: alpha))
    }
    
    static func customDynamicColor(_ customColor: Custom) -> Color {
        return Color(customColor.rawValue)
    }
    
}

