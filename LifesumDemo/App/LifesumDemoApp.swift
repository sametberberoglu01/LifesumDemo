//
//  LifesumDemoApp.swift
//  LifesumDemo
//
//  Created by Samet Berberoğlu on 30.06.2022.
//

import SwiftUI

@main
struct LifesumDemoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
