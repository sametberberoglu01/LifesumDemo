//
//  DebugLogger.swift
//  LifesumDemo
//
//  Created by Samet Berberoğlu on 2.07.2022.
//

import Foundation

final class DebugLogger {
    
    private static let serialQueue = DispatchQueue(label: "DebugLoggerSerialQueue")
    
    class func log(string: String?) {
    #if DEBUG
        serialQueue.sync {
            guard let string = string else { return }
            print("\n\(string)")
        }
    #endif
    }
    
    class func log(response: URLResponse?, data: Data?, httpMethod: String?) {
    #if DEBUG
        let httpResponse = response as? HTTPURLResponse
        let url = "<<<< Response (\(httpMethod ?? "")) : (\(httpResponse?.statusCode ?? -1)) : \(response?.url?.absoluteString ?? "")"
        let body = data?.prettyPrintedJsonString ?? ""
        log(string: "\n\(url)\n\(body)")
    #endif
    }
    
    class func log(response: URLResponse?, error: Error?, httpMethod: String?) {
    #if DEBUG
        let httpResponse = response as? HTTPURLResponse
        let url = "<<<< Response (\(httpMethod ?? "")) : (\(httpResponse?.statusCode ?? -1)) : \(response?.url?.absoluteString ?? "")"
        let error = error?.localizedDescription ?? ""
        log(string: "\n\(url)\n\(error)")
    #endif
    }
    
    class func logIncorrectObject(_ object: Any?) {
    #if DEBUG
        guard let object = object else { return }
        log(string: "\nIncorrect Response Object: \(type(of: object))")
    #endif
    }
    
    class func log(request: URLRequest) {
    #if DEBUG
        let url = ">>>> Request (\(request.httpMethod ?? "")) : \(request.url?.absoluteString ?? "")"
        var logStr = "\n\(url)"
        if let header = request.allHTTPHeaderFields, !header.isEmpty {
            logStr += "\nHeader Fields:\n\(header.prettyPrintedJsonString)"
        }
        if let body = request.httpBody?.prettyPrintedJsonString {
            logStr += "\nHttp Body:\n\(body)\n"
        }
        log(string: logStr)
    #endif
    }
    
}

private extension Data {
    
    var prettyPrintedJsonString: String {
        guard let object = try? JSONSerialization.jsonObject(with: self, options: []),
              let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]),
              let prettyPrintedString = String(data: data, encoding: .utf8) else { return "" }
        return prettyPrintedString
    }
    
}

private extension Collection {
    
    var prettyPrintedJsonString: String {
        guard let jsonData = try? JSONSerialization.data(withJSONObject: self, options: [.prettyPrinted]) else { return "" }
        return String(data: jsonData, encoding: .utf8) ?? ""
    }
    
}
