//
//  NetworkError.swift
//  LifesumDemo
//
//  Created by Samet Berberoğlu on 2.07.2022.
//

import Foundation

enum NetworkError: Error {
    
    case operationCancelled
    case requestFailed(error: Error)
    case unknownStatusCode
    case unexpectedStatusCode(code: Int)
    case contentEmptyData
    case contentDecoding(error: Error)
    
}

extension NetworkError: LocalizedError {
    
    var errorDescription: String? {
        switch self {
        case .operationCancelled:
            return "operation_was_cancelled".localized
        case let .requestFailed(error):
            return "request_failed".localizedWithArguments(error.localizedDescription)
        case .unknownStatusCode:
            return "unknown_status_code".localized
        case let .unexpectedStatusCode(code):
            return "unexpected_status_code".localizedWithArguments(code)
        case .contentEmptyData:
            return "content_empty_data".localized
        case let .contentDecoding(error):
            return "decoding_error".localizedWithArguments(error.localizedDescription)
        }
    }
    
}
