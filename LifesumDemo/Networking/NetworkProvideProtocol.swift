//
//  NetworkProvideProtocol.swift
//  LifesumDemo
//
//  Created by Samet Berberoğlu on 2.07.2022.
//

import Foundation

protocol NetworkProviderProtocol {
    
    var urlSession: SessionProtocol { get }
    var service: NetworkService { get }
    
    func submit<T: Decodable>() async throws -> T
    
}

extension NetworkProviderProtocol {
    
    func submit<T: Decodable>() async throws -> T {
        return try await urlSession.submitRequest(service.urlRequest)
    }
    
}
