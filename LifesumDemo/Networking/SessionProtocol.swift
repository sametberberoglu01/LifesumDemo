//
//  SessionProtocol.swift
//  LifesumDemo
//
//  Created by Samet Berberoğlu on 2.07.2022.
//

import Foundation

protocol SessionProtocol {
    
    func submitRequest<T: Decodable>( _ request: URLRequest) async throws -> T
    
}
