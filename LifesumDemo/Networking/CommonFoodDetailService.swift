//
//  CommonFoodDetailService.swift
//  LifesumDemo
//
//  Created by Samet Berberoğlu on 2.07.2022.
//

import Foundation

enum CommonFoodDetailService {
    
    static let baseURL: String? = {
        guard let apiBaseURL: String = try? PlistReader.value(for: "API_BASE_URL") else { return nil }
        return apiBaseURL
    }()
    
}
