//
//  FoodDetailServiceClient.swift
//  LifesumDemo
//
//  Created by Samet Berberoğlu on 2.07.2022.
//

import Foundation

struct FoodDetailServiceClient: NetworkProviderProtocol {
    
    let urlSession: SessionProtocol
    let service: NetworkService
    
    init(urlSession: SessionProtocol = Session(), clientService: NetworkService) {
        self.urlSession = urlSession
        self.service = clientService
    }
    
}
