//
//  FoodDetailService.swift
//  LifesumDemo
//
//  Created by Samet Berberoğlu on 2.07.2022.
//

import Foundation

struct FoodDetailService: NetworkService {
    
    let foodId: Int
    
    var baseURL: String? { CommonFoodDetailService.baseURL }
    
    var path: String { "/v2/foodipedia/codetest" }
    
    var method: HttpMethod { .get }
    
    var httpBody: Encodable? { nil }
    
    var headers: [String : String]? { nil }
    
    var queryParameters: [URLQueryItem]? {
        return [URLQueryItem(name: "foodid", value: "\(foodId)")]
    }
    
    var timeout: TimeInterval? { NetworkEnvironment.requestDefaultTimeout }
    
    
}
