//
//  FoodDetailRepository.swift
//  LifesumDemo
//
//  Created by Samet Berberoğlu on 2.07.2022.
//

import Foundation

protocol FoodDetailRepositoryProtocol {
    var client: NetworkProviderProtocol? { get set }
    
    @MainActor
    func fetchFoodDetail(foodId: Int) async throws -> FoodDetailModel
}

final class FoodDetailRepository: FoodDetailRepositoryProtocol {
    
    var client: NetworkProviderProtocol?
    
    init(client: NetworkProviderProtocol? = nil) {
        self.client = client
    }
    
    func fetchFoodDetail(foodId: Int) async throws -> FoodDetailModel {
        if client == nil {
            client = FoodDetailServiceClient(clientService: FoodDetailService(foodId: foodId))
        }
        async let foodDetail: FoodDetail = try fetchFoodDetail(client: client)
        return try await foodDetail.convertToFoodDetailModel()
    }
    
}

private extension FoodDetailRepository {
    
    func fetchFoodDetail<T: Decodable>(client: NetworkProviderProtocol?) async throws -> T {
        defer { self.client = nil }
        guard let client = client else {
            throw NetworkError.operationCancelled
        }
        return try await client.submit()
    }
    
}
