//
//  Session.swift
//  LifesumDemo
//
//  Created by Samet Berberoğlu on 2.07.2022.
//

import Foundation

final class Session: SessionProtocol {
    
    private let session: URLSession
    
    nonisolated init(session: URLSession = .shared) {
        self.session = session
    }
    
    func submitRequest<T: Decodable>( _ request: URLRequest) async throws -> T {
        DebugLogger.log(request: request)
        
        var _response: URLResponse?
        var _data: Data?
        
        do {
            let (data, response) = try await session.data(for: request)
            _response = response
            _data = data
            try validate(response: response)
            let decodedResponse = try decode(data: data, type: NetworkResponse<T>.self)
            guard let model = decodedResponse.response else {
                DebugLogger.log(response: response, error: NetworkError.contentEmptyData, httpMethod: request.httpMethod)
                throw NetworkError.contentEmptyData
            }
            DebugLogger.log(response: response, data: data, httpMethod: request.httpMethod)
            return model
        } catch {
            DebugLogger.log(response: _response, data: _data, httpMethod: request.httpMethod)
            DebugLogger.log(response: _response, error: error, httpMethod: request.httpMethod)
            throw error
        }
    }
    
}

private extension Session {
    
    func validate(response: URLResponse?) throws {
        guard let httpResponse = response as? HTTPURLResponse else {
            throw NetworkError.unknownStatusCode
        }
        
        if !NetworkEnvironment.successStatusCodeRange.contains(httpResponse.statusCode) {
            throw NetworkError.unexpectedStatusCode(code: httpResponse.statusCode)
        }
    }
    
    func decode<T: Decodable>(data: Data?, type: T.Type) throws -> T {
        guard let data = data, !data.isEmpty else {
            throw NetworkError.contentEmptyData
        }
        
        do {
            let decoder = JSONDecoder()
            return try decoder.decode(type, from: data)
        } catch {
            throw NetworkError.contentDecoding(error: error)
        }
    }
    
}
