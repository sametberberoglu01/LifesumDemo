//
//  NetworkEnvironment.swift
//  LifesumDemo
//
//  Created by Samet Berberoğlu on 2.07.2022.
//

import Foundation

typealias NetworkResultCompletion<T: Decodable> = (Result<T, Error>) -> Void

enum NetworkEnvironment {
    static let successStatusCodeRange: Range<Int> = 200 ..< 300
    static let requestDefaultTimeout: TimeInterval = 60
}

enum HttpMethod: String {
    case get = "GET"
    case post = "POST"
    case patch = "PATCH"
    case delete = "DELETE"
}
