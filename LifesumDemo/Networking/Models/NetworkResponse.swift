//
//  NetworkResponse.swift
//  LifesumDemo
//
//  Created by Samet Berberoğlu on 3.07.2022.
//

import Foundation

struct NetworkResponse<T: Decodable>: Decodable {
    let meta: NetworkResponseMeta
    var response: T?
}

struct NetworkResponseMeta: Decodable {
    let code: Int
    var errorType: String?
    var errorDetail: String?
}
