//
//  FoodDetailModel.swift
//  LifesumDemo
//
//  Created by Samet Berberoğlu on 2.07.2022.
//

import Foundation

struct FoodDetail: Decodable {
    
    let title: String
    let calories: Double
    let carbs: Double
    let protein: Double
    let fat: Double
    let gramsPerServing: Double
    
    private enum CodingKeys: String, CodingKey {
        case title, calories, carbs, protein, fat
        case gramsPerServing = "gramsperserving"
    }
    
}

extension FoodDetail {
    func convertToFoodDetailModel() -> FoodDetailModel {
        return FoodDetailModel(foodDetail: self)
    }
}

final class FoodDetailModel: ObservableObject {
    
    @Published var title: String
    @Published var calories: String
    @Published var ingredientItems: [IngredientItem] = []
    @Published var caloriesText: String
    
    private var carbs: String
    private var protein: String
    private var fat: String
    
    init(foodDetail: FoodDetail) {
        self.title = foodDetail.title
        self.calories = String(format: "%.0f", foodDetail.calories)
        self.carbs = String(format: "%.1f", foodDetail.carbs) + "%"
        self.protein = String(format: "%.1f", foodDetail.protein) + "%"
        self.fat = String(format: "%.1f%", foodDetail.fat) + "%"
        self.caloriesText = "calories_per_serving".localizedWithArguments(foodDetail.gramsPerServing)
        ingredientItems = [.init(title: "carbs".localized, value: self.carbs),
                           .init(title: "protein".localized, value: self.protein),
                           .init(title: "fat".localized, value: self.fat)]
    }
    
    #if DEBUG
    convenience init() {
        self.init(foodDetail: Self.foodDetailMockModel)
    }
    #endif
}

final class IngredientItem: ObservableObject, Equatable, Identifiable {
    
    @Published var title: String
    @Published var value: String
    let id: String
    
    init(title: String, value: String) {
        self.title = title
        self.value = value
        self.id = title + value
    }
    
    static func == (lhs: IngredientItem, rhs: IngredientItem) -> Bool {
        return lhs.value == rhs.value && lhs.title == rhs.title
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(value)
        hasher.combine(title)
    }
}

private extension FoodDetailModel {
    
    static let foodDetailMockModel: FoodDetail = {
        let decoder = JSONDecoder()
        let path = Bundle.main.path(forResource: "FoodDetail", ofType: "json")!
        let data = try! Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
        return try! decoder.decode(FoodDetail.self, from: data)
    }()
    
}
