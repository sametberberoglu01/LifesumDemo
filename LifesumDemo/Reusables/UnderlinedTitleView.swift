//
//  UnderlinedTitleView.swift
//  LifesumDemo
//
//  Created by Samet Berberoğlu on 2.07.2022.
//

import SwiftUI

struct UnderlinedTitleView: View {
    
    let title: String?
    
    var body: some View {
        VStack(spacing: 0.0, content: {
            Text(title?.uppercased() ?? "")
                .multilineTextAlignment(.center)
                .foregroundColor(.white)
                .frame(maxWidth: 240.0)
                .lineLimit(2)
                .minimumScaleFactor(0.7)
                .font(.avenir(weight: .regular, size: 22.0))
            
            Capsule(style: .circular)
                .fill(.white)
                .frame(maxWidth: .infinity, maxHeight: 2.0)
        })
        .fixedSize(horizontal: true, vertical: false)
        .frame(maxWidth: .infinity)
    }
}

struct UnderlinedTitleView_Previews: PreviewProvider {
    static var previews: some View {
        UnderlinedTitleView(title: "Cashews")
            .previewLayout(.sizeThatFits)
    }
}
