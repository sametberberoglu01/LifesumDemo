//
//  BottomButtonView.swift
//  LifesumDemo
//
//  Created by Samet Berberoğlu on 2.07.2022.
//

import SwiftUI

struct BottomButtonView: View {
    
    let title: String
    let size: CGSize
    let actionHandler: () -> Void
    
    var body: some View {
        Button(action: {
            actionHandler()
        }){
            Text(title)
                .font(.avenir(weight: .regular, size: 18.0))
                .frame(width: size.width, height: size.height)
                .foregroundColor(.customDynamicColor(.ls_primaryButtonText))
                .background(Color.customDynamicColor(.ls_primaryButton))
                .clipShape(Capsule())
                .shadow(color: Color(white: 0, alpha: 0.33), radius: 16.0, x: 0.0, y: 16.0)
        }
        .buttonStyle(PlainButtonStyle())
    }
    
}

struct BottomButtonView_Previews: PreviewProvider {
    static var previews: some View {
        BottomButtonView(title: "MORE INFO", size: CGSize(width: 300.0, height: 75.0)) {}
        .previewLayout(.sizeThatFits)
    }
}
