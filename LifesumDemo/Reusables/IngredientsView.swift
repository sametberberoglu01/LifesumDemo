//
//  IngredientsView.swift
//  LifesumDemo
//
//  Created by Samet Berberoğlu on 2.07.2022.
//

import SwiftUI

struct IngredientsView: View {
    
    let items: [IngredientItem]?
    
    var body: some View {
        HStack(spacing: 10.0) {
            ForEach(items ?? []) { item in
                IngredientView(item: item)
                    .scaledToFit()
            }
        }
    }
}

struct IngredientsView_Previews: PreviewProvider {
    static var previews: some View {
        IngredientsView(items: [.init(title: "CARBS", value: "10%"),
                                .init(title: "PROTEIN", value: "60%"),
                                .init(title: "FAT", value: "10%")])
        .previewLayout(.sizeThatFits)
    }
}


private struct IngredientView: View {
    
    let item: IngredientItem
    
    var body: some View {
        VStack(spacing: 2.0, content: {
            Text(item.title)
                .multilineTextAlignment(.center)
                .foregroundColor(.customDynamicColor(.ls_grayText))
                .minimumScaleFactor(0.7)
                .font(.avenir(weight: .regular, size: 14.0))
            
            Capsule(style: .circular)
                .fill(Color.customDynamicColor(.ls_grayText))
                .frame(maxWidth: .infinity, maxHeight: 1.0)
            
            Text(item.value)
                .multilineTextAlignment(.center)
                .foregroundColor(.customDynamicColor(.ls_grayText))
                .minimumScaleFactor(0.7)
                .font(.avenir(weight: .regular, size: 14.0))
        })
        .fixedSize(horizontal: true, vertical: false)
    }
}
