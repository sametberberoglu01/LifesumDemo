//
//  GradientCircleView.swift
//  LifesumDemo
//
//  Created by Samet Berberoğlu on 2.07.2022.
//

import SwiftUI

struct GradientCircleView: View {
    
    let colors: [Color]
    let dimemsion: CGFloat
    let startPoint: UnitPoint
    let endPoint: UnitPoint
    let shadowColor: Color
    
    var body: some View {
        Circle()
            .fill(LinearGradient(gradient: Gradient(colors: colors),
                                 startPoint: startPoint,
                                 endPoint: endPoint))
            .frame(width: dimemsion, height: dimemsion)
            .shadow(color: shadowColor, radius: 8.0, x: 0, y: 8.0)
    }
}

struct GradientCircleView_Previews: PreviewProvider {
    static var previews: some View {
        GradientCircleView(colors: [.customDynamicColor(.ls_orange),
                                    .customDynamicColor(.ls_grapefruit)],
                           dimemsion: 300.0,
                           startPoint: .topLeading,
                           endPoint: .bottomTrailing,
                           shadowColor: .customDynamicColor(.ls_orangeyShadow))
        .previewLayout(.sizeThatFits)
    }
}
