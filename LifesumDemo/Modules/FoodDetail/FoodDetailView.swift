//
//  FoodDetailView.swift
//  LifesumDemo
//
//  Created by Samet Berberoğlu on 1.07.2022.
//

import SwiftUI

struct FoodDetailView: View {
    
    fileprivate enum Constants {
        static let elementWidth: CGFloat = 300.0
        static let buttonHeight: CGFloat = 75.0
        static let padding: CGFloat = 20.0
    }
    
    @ObservedObject var viewModel: FoodDetailViewModel
    
    init(viewModel: FoodDetailViewModel = FoodDetailViewModel()) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        ZStack {
            VStack {
                Group {
                    FoodCircleView(foodDetailModel: viewModel.foodDetailModel)
                    IngredientsView(items: viewModel.foodDetailModel?.ingredientItems)
                        .padding(.top, Constants.padding)
                }
                
                Spacer()
                
                BottomButtonView(title: "more_info".localized,
                                 size:CGSize(width: Constants.elementWidth, height: Constants.buttonHeight),
                                 actionHandler: {
                    Task {
                        await viewModel.loadData()
                    }
                })
                .disabled(viewModel.isFetching)
            }
            .padding([.top, .bottom], Constants.padding)
            .alert(isPresented: $viewModel.showsError, content: {
                Alert(
                    title: Text("warning".localized),
                    message: Text(viewModel.errorMessage)
                )
            })
            
            ProgressView()
                .progressViewStyle(.circular)
                .setHidden(!viewModel.isFetching)
        }
    }
    
}

struct FoodDetailView_Previews: PreviewProvider {
    static var previews: some View {
        FoodDetailView()
    }
}

private struct FoodCircleView: View {
    
    let foodDetailModel: FoodDetailModel?
    
    private let gradientCircleView: GradientCircleView = GradientCircleView(
        colors: [.customDynamicColor(.ls_orange), .customDynamicColor(.ls_grapefruit)],
        dimemsion: FoodDetailView.Constants.elementWidth,
        startPoint: .topLeading,
        endPoint: .bottomTrailing,
        shadowColor: .customDynamicColor(.ls_orangeyShadow))
    
    private func foodCaloriesView() -> some View {
        return VStack(spacing: 0.0, content: {
            Text(foodDetailModel?.calories ?? "")
                .font(.avenir(weight: .regular, size: 66))
                .foregroundColor(.white)
            Text(foodDetailModel?.caloriesText ?? "")
                .foregroundColor(.white)
                .font(.avenir(weight: .regular, size: 16).italic())
        })
    }
    
    var body: some View {
        ZStack {
            gradientCircleView
            VStack(spacing: 10.0, content: {
                UnderlinedTitleView(title: foodDetailModel?.title)
                foodCaloriesView()
            })
        }
        .setHidden(foodDetailModel == nil)
    }
    
}
