//
//  FoodDetailViewModel.swift
//  LifesumDemo
//
//  Created by Samet Berberoğlu on 2.07.2022.
//

import Foundation

protocol FoodDetailViewModelProtocol {
    var repository: FoodDetailRepositoryProtocol { get set }
    func loadData(foodId: Int?) async
}

final class FoodDetailViewModel: FoodDetailViewModelProtocol, ObservableObject {
    
    @Published private(set) var foodDetailModel: FoodDetailModel?
    @Published var showsError = false
    @Published private(set) var errorMessage = ""
    @Published private(set) var isFetching = false
    
    var repository: FoodDetailRepositoryProtocol
    // api provides 200 foods, 10 foods are for error cases.
    private let foodIds = Array(1...210)
    
    private var randomFoodId: Int {
        return foodIds.randomElement ?? 1
    }
    
    init(repository: FoodDetailRepositoryProtocol = FoodDetailRepository()) {
        self.repository = repository
    }
    
    func loadData(foodId: Int? = nil) async {
        guard let repository = repository as? FoodDetailRepository else { return }
        isFetching = true
        do {
            foodDetailModel = try await repository.fetchFoodDetail(foodId: foodId ?? randomFoodId)
        } catch {
            errorMessage = error.localizedDescription
            showsError = true
        }
        isFetching = false
    }
    
}
