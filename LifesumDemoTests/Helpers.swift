//
//  Helpers.swift
//  LifesumDemoTests
//
//  Created by Samet Berberoğlu on 3.07.2022.
//

import XCTest
@testable import LifesumDemo

class Helper {
    
    static let urlSession: SessionProtocol = {
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [MockUrlProtocol.self]
        let session = URLSession(configuration: configuration)
        return Session(session: session)
    }()
    
    class func generateDataFrom(fileName: String) -> Data {
        let path = Bundle(for: Self.self).path(forResource: fileName, ofType: "json")
        XCTAssertNotNil(path, "the file can not be found in the current bundle")
        let data = try? Data(contentsOf: URL(fileURLWithPath: path!), options: .mappedIfSafe)
        XCTAssertNotNil(data, "data conversion failure")
        return data!
    }
    
    class func decoded<T: Decodable>(dataType: T.Type, from data: Data) -> T? {
        let decoder = JSONDecoder()
        let decodedObject = try? decoder.decode(NetworkResponse<T>.self, from: data)
        XCTAssertNotNil(decodedObject, "decodingError")
        return decodedObject?.response
    }
    
}
