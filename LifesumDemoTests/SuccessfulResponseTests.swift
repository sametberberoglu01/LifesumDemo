//
//  SuccessfulResponseTests.swift
//  LifesumDemoTests
//
//  Created by Samet Berberoğlu on 30.06.2022.
//

import XCTest
@testable import LifesumDemo

class SuccessfulResponseTests: XCTestCase {

    private lazy var foodDetailRespository: FoodDetailRepositoryProtocol = FoodDetailRepository(client: self)
    private lazy var foodDetailViewModel: FoodDetailViewModelProtocol = FoodDetailViewModel(repository: foodDetailRespository)
    private let _urlSession = Helper.urlSession
    
    private let foodId = 66
    
    func testSuccessfulResponse() async {
        let data = Helper.generateDataFrom(fileName: "SuccessfulResponse")
        let referenceFoodDetail = Helper.decoded(dataType: FoodDetail.self, from: data)?.convertToFoodDetailModel()
        await prepareForTestAndLoadData(with: data)
        guard let viewModel = foodDetailViewModel as? FoodDetailViewModel,
              let foodDetailModel = viewModel.foodDetailModel,
              let referenceFoodDetail = referenceFoodDetail else {
            XCTFail("found unexpected nil value")
            return
        }
        
        XCTAssertEqual(foodDetailModel.title, referenceFoodDetail.title)
        XCTAssertEqual(foodDetailModel.calories, referenceFoodDetail.calories)
        XCTAssertEqual(foodDetailModel.caloriesText, referenceFoodDetail.caloriesText)
        XCTAssertEqual(foodDetailModel.ingredientItems, referenceFoodDetail.ingredientItems)
        XCTAssertFalse(viewModel.showsError)
        XCTAssertTrue(viewModel.errorMessage.isEmpty)
    }
    
    //MARK: Helpers
    private func prepareForTestAndLoadData(with referenceData: Data) async {
        MockUrlProtocol.testSamples = [service.urlRequest.url: referenceData]
        (foodDetailViewModel.repository as? FoodDetailRepository)?.client = self
        await foodDetailViewModel.loadData(foodId: foodId)
    }

}

extension SuccessfulResponseTests: NetworkProviderProtocol {
    
    var urlSession: SessionProtocol {
        return _urlSession
    }
    
    var service: NetworkService {
        return FoodDetailService(foodId: foodId)
    }
    
}
