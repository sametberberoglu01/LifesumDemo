//
//  UnsuccessfulResponseTests.swift
//  LifesumDemoTests
//
//  Created by Samet Berberoğlu on 4.07.2022.
//

import XCTest
@testable import LifesumDemo

class UnsuccessfulResponseTests: XCTestCase {
    
    private lazy var foodDetailRespository: FoodDetailRepositoryProtocol = FoodDetailRepository(client: self)
    private lazy var foodDetailViewModel: FoodDetailViewModelProtocol = FoodDetailViewModel(repository: foodDetailRespository)
    private let _urlSession = Helper.urlSession
    
    private let foodId = 99999
    
    func testUnsuccessfulResponse() async {
        let data = Helper.generateDataFrom(fileName: "UnsuccesfulResponse")
        let referenceFoodDetail = Helper.decoded(dataType: FoodDetail.self, from: data)?.convertToFoodDetailModel()
        await prepareForTestAndLoadData(with: data)
        let viewModel = foodDetailViewModel as? FoodDetailViewModel
        
        XCTAssertNil(referenceFoodDetail)
        XCTAssertNotNil(viewModel?.errorMessage)
        XCTAssertTrue(viewModel!.showsError)
        XCTAssertNil(viewModel?.foodDetailModel)
    }
    
    //MARK: Helpers
    private func prepareForTestAndLoadData(with referenceData: Data) async {
        MockUrlProtocol.testSamples = [service.urlRequest.url: referenceData]
        (foodDetailViewModel.repository as? FoodDetailRepository)?.client = self
        await foodDetailViewModel.loadData(foodId: foodId)
    }
    
}

extension UnsuccessfulResponseTests: NetworkProviderProtocol {
    
    var urlSession: SessionProtocol {
        return _urlSession
    }
    
    var service: NetworkService {
        return FoodDetailService(foodId: foodId)
    }
    
}

